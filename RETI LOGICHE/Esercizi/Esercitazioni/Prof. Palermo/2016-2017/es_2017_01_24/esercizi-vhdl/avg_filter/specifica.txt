Realizzare la specifica VHDL del componente in figura (pdf allegato, le porte di reset e clock dei registri non sono state disegnate per non complicare troppo il disegno). In particolare il circuito ha un ingresso I a 32 bit ed un'uscita a Y a 32 bit. Ad ogni ciclo di clock, il circuito presenta in uscita la media degli ultimi quattro valori ricevuti in ingresso (i valori in ingresso sono numeri naturali in codifica binaria). Per semplicità si approssimi la media con la seguente funzione:
       3
Y[t]= SUM (I[t-i]/4)
      i=0
      
Si ipotizzi di avere a disposizione i seguenti componenti:

--registro a 32 bit
entity reg32 is
  port(d: in std_logic_vector(31 downto 0);
      clk, rst: in std_logic;
      q: out std_logic_vector(31 downto 0));
end reg32;

--sommatore a 32 bit con carryin e carryout
entity adder32 is
	port(A, B : in std_logic_vector(31 downto 0);
			S : out std_logic_vector(31 downto 0);
			Cin : in std_logic;
			Cout: out std_logic);
end adder32;


